//const { nipplejs } = require("./nipple");

const canvas = document.getElementById("d_canvas")
const context = canvas.getContext("2d");
let username = localStorage.getItem('username');

const scale = 5;
const width = 17;
const height = 17;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 1, 0, 0];
const frameLimit = 16;
const score = 0;


let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 1.5;
let scoreCount = 0;
if (score)
{
    scoreCount = score;
}


//assigning player image
let playerImage = new Image();
playerImage.src = "assets/media/black-mage-overworld-sprite.png";

function drawFrame(playerImage, frameX, frameY, canvasX, canvasY) {
    context.drawImage(playerImage,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function addName() {
    let header = document.getElementById("main-header");
    header.innerHTML = "Hello " + username;
}

addName();

// GameObject holds positional information
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    //this.mvmtDirection = "None";
}

// Default Player
let player = new GameObject(playerImage, 0, 0, 200, 200);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

function clickableDpadReleased() {
    console.log(event);
}
function clickDpadYellow(){
    console.log(event);
}
function clickDpadBlue(){
    console.log(event);
}
function clickDpadRed(){
    console.log(event);
}
function clickDpadGreen(){
    console.log(event);
}
let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];


// Take Input from the Player
function input(event) {
    
    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break;
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            case 49: //Number 1
                gamerInput = new GamerInput("1");
            break; 
            case 50: //Number 2 
                gamerInput = new GamerInput("2");
            break;
            case 51: //Number 3
                gamerInput = new GamerInput("3");
            break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}


function changeCharacter()
{
    if (gamerInput.action === "1")
        {
            playerImage.src = "assets/media/black-mage-overworld-sprite.png";

        }
    if (gamerInput.action === "2")
        {
            playerImage.src = "assets/media/fighter-overworld-sprite.png";
        }
    if (gamerInput.action === "3")
        {
            playerImage.src = "assets/media/white-mage-overworld-sprites.png";
        }
}

function update() {

    changeCharacter();

    // Move Player Up
    if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.y -= speed;
        currentDirection = 2;
    }
    // Move Player Down
    else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.y += speed;
        currentDirection = 0; 
    }
    // Move Player Left 
    else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.x -= speed;
        currentDirection = 1; 
    }
    // Move Player Right
    else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.x += speed;
        currentDirection = 3; 
    }

}

function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}


function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    //console.log(player);
    
    drawHealthbar();
    animate();
    writeScore();
}

function writeScore()
{
    let scoreString = "score: " + scoreCount;
    context.font = "22px sans-serif";
    context.fillStyle = "pink";
    context.fillText(scoreString, 890, 60);
}

var fillVal = 0;

function drawHealthbar() {
    var width = 1100;
    var height = 25;
    var max = 100;
    var val = 10;
  
    // Draw the background
    context.fillStyle = "#000000";
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(0, 0, width, height);
  
    // Draw the fill
    context.fillStyle = "#00FF00";
    context.fillRect(0, 0, fillVal * width, height);
    fillVal += 0.0005;
}
  
var dynamic = nipplejs.create({
    color: 'pink',
    zone: document.getElementById("joy")
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
     });
  });

function clickDpadYellow()
{
    gamerInput = new GamerInput("Up");
}
function clickDpadBlue()
{
    gamerInput = new GamerInput("Left");
}
function clickDpadRed()
{
    gamerInput = new GamerInput("Right");
}
function clickDpadGreen()
{
    gamerInput = new GamerInput("Down");
}

function stopClick()
{
    gamerInput = new GamerInput("None");
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);

// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);